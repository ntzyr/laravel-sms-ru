<?php


namespace BonchDev\LaravelSMSRU;


use Illuminate\Support\ServiceProvider;
use BonchDev\SMSRUSDK\SMS;

class SMSRUServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/sms.ru.php', 'sms.ru'
        );

        $this->app->bind(SMS::class, function () {
            return new SMS(
                config('sms.ru.api_id'),
                true,
                false
            );
        });
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/sms.ru.php' => config_path('sms.ru.php')
        ], 'sms.ru-config');
    }
}