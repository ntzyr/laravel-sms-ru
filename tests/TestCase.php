<?php


namespace Ntzyr\LaravelSMSRU\Tests;


use Ntzyr\LaravelSMSRU\SMSRUServiceProvider;
use Orchestra\Testbench\Concerns\CreatesApplication;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
    }

    protected function getPackageProviders($app)
    {
        return [
            SMSRUServiceProvider::class
        ];
    }
}